#!/usr/bin/env python3
# -*- coding: utf-8 -*-

PGRNAME="Impedance File Viewer"
VERSION="v1.34"

import os, sys, time, math, base64

try:
	# sudo apt-get install python3-pyqt5
	from PyQt5.QtGui import *
	from PyQt5.QtCore import *
	from PyQt5.QtWidgets import *
	QT_VERSION = "PyQt5"
except:
	# sudo apt-get install python-qtpy python3-qtpy
	from PyQt4.QtGui import *
	from PyQt4.QtCore import *
	QT_VERSION = "PyQt4"

import numpy, pyqtgraph # PyQt must be imported before pyqtgraph

def isShiftPressed():
	mods = QApplication.keyboardModifiers()
	isShiftPressed =  mods & Qt.ShiftModifier
	return bool(isShiftPressed)

def str_f(something):
	try:    return str(something) # Python 3
	except: pass
	try:
		if type(something) == QString: # Python2 and Qt4, tested on XP with French accentuated characters
			return str(something.toUtf8()).decode('utf-8')
	except: pass
	try:    return something.encode('utf-8') # Python2 and PyQt5 (or Python3 with PyQt4 or 5)
	except: pass
	try:    return str(something.toUtf8()) # Python2 and PyQt4
	except: pass

def str_u(something):
	try:    return unicode(something, 'utf-8') # Python 2
	except: pass
	return something # Python 3

DEFAULT_EXPORT = 0; EC_LAB_EXPORT = 1

class ImpedanceViewerGUI(QWidget):
	def __init__(self, app):
		# Set up everything
		self.filename = ""
		QWidget.__init__(self)

		self.initUI()
		self.applyTheme()

		# Infinite lines related variables
		self.infline = []
		self.inflineCurrentlyDragged = False
		self.inflineLastDragged = False

		# Load optionnal data file
		for arg in app.arguments()[1:]:
			if arg[0] == '-':
				continue
			ext = os.path.splitext(str_f(arg))[1].lower()
			if ext == '.py' or ext == '.pyc' or ext == '.exe':
				continue
			self.loadImpedanceFile(arg)
			break

	def applyTheme(self, darkTheme=True):
		if darkTheme:  bg = '#000000'; fg = '#ffffff'
		else:          bg = '#ffffff'; fg = '#000000'

		pyqtgraph.setConfigOption('background', bg)
		pyqtgraph.setConfigOption('foreground', fg)

		for pw in self.pw_temperature, self.pw_nyquist, self.pw_bodemag, self.pw_bodeph:
			try:
				pw.setBackground(bg)
				pw.getAxis('left').setTextPen(fg) # doesn't work with old version of pyqtgraph
				pw.getAxis('bottom').setTextPen(fg)
			except:
				pass

			try:
				for i, item in enumerate(pw.plotItem.items):
					item.setPen(self.colorChoice(i) if len(self.infline) > 1 and pw != self.pw_temperature else fg)
			except:
				pass

		self.blackWidget.setStyleSheet("\
			QWidget { background-color: %s; color: %s; }\
			QSplitter::handle { background-color: %s; }\
		" % (bg, fg, bg))

		if self.filename != "":
			for i, infline in enumerate(self.infline):
				infline.setPen(self.colorChoice(i))
				infline.label.setColor(self.colorChoice(i))

	def colorChoice(self, i):
		colors = [(240, 240, 80),
				  (  0, 240, 240),
				  (240,   0, 240),
				  (  0, 240,   0),
				  (200, 110,   0),
				  (255,   0,   0),
				  (255,  70,   0),
				  (255, 255,   0),
				  (  0, 255,   0),
				  (100, 100, 255),
				  (138,  43, 226),
				  (127, 127, 127),
				  (255, 255, 255)]
		if self.button_whitetheme.isChecked():
			return (0, 0, 0)
		return colors[i%len(colors)]

	def initUI(self):
		self.setStyleSheet("\
			QLabel#removedPointsLabel { color: #ff0000; font-weight: bold; } \
			QSplitter::handle { image:None; }\
		")

		mainvboxLayout = QVBoxLayout(self)
		mainvboxLayout.setContentsMargins(0, 5, 0, 0)
		self.blackWidget = QWidget()

		# splitter between values and first graph
		splitterLeft   = QSplitter(Qt.Vertical)
		splitterRight  = QSplitter(Qt.Vertical)
		splitterMiddle = QSplitter(Qt.Horizontal)
		splitterMiddle.addWidget(splitterLeft)
		splitterMiddle.addWidget(splitterRight)

		# top part of the window
		boxTop = QHBoxLayout()
		boxTop.setContentsMargins(5, 0, 5, 0)

		# pushbutton "A" (autoRange)
		self.button_autorange = QToolButton()
		self.button_autorange.setFocusPolicy(Qt.TabFocus)
		self.button_autorange.setText(u'A')
		self.button_autorange.setStyleSheet("width: 1em; ")
		self.button_autorange.setToolTip(u"Reset plots scales auto-range states (Ctrl-A)")
		self.button_autorange.clicked.connect(self.autoRange)
		boxTop.addWidget(self.button_autorange)

		# pushbutton "1B"
		self.button_onebuttonmode = QToolButton()
		self.button_onebuttonmode.setFocusPolicy(Qt.TabFocus)
		self.button_onebuttonmode.setCheckable(True)
		self.button_onebuttonmode.setText(u'1b')
		self.button_onebuttonmode.setStyleSheet("width: 1em; ")
		self.button_onebuttonmode.setToolTip(u"One-button mode for plots")
		self.button_onebuttonmode.toggled.connect(self.oneButtonModeToggled)
		boxTop.addWidget(self.button_onebuttonmode)

		# pushbutton "W"
		self.button_whitetheme = QToolButton()
		self.button_whitetheme.setFocusPolicy(Qt.TabFocus)
		self.button_whitetheme.setCheckable(True)
		self.button_whitetheme.setText(u'W')
		self.button_whitetheme.setStyleSheet("width: 1em; ")
		self.button_whitetheme.setToolTip(u"Turn the 'White background' mode")
		self.button_whitetheme.toggled.connect(lambda: self.applyTheme(darkTheme=not self.button_whitetheme.isChecked()))
		boxTop.addWidget(self.button_whitetheme)

		# pushbutton "0"
		self.button_nyquistzero = QToolButton()
		self.button_nyquistzero.setFocusPolicy(Qt.TabFocus)
		self.button_nyquistzero.setCheckable(True)
		self.button_nyquistzero.setText(u'0')
		self.button_nyquistzero.setStyleSheet("width: 1em; ")
		self.button_nyquistzero.setToolTip(u"Be certain to always show (0,0) on the Nyquist plot")
		self.button_nyquistzero.setDisabled(True)
		self.button_nyquistzero.toggled.connect(self.measurementnbrChangedEvent)
		boxTop.addWidget(self.button_nyquistzero)

		# pushbutton "+"
		self.button_add = QToolButton()
		self.button_add.setFocusPolicy(Qt.TabFocus)
		self.button_add.setText(u'+')
		self.button_add.setStyleSheet("width: 1em; ")
		self.button_add.setToolTip(u"Add a marker line in the temperature plot")
		self.button_add.setDisabled(True)
		self.button_add.released.connect(self.addInfiniteLine)
		boxTop.addWidget(self.button_add)

		# pushbutton "-"
		self.button_delete = QToolButton()
		self.button_delete.setFocusPolicy(Qt.TabFocus)
		self.button_delete.setText(u'-')
		self.button_delete.setStyleSheet("width: 1em; ")
		self.button_delete.setToolTip(u"Delete a marker line in the temperature plot")
		self.button_delete.setDisabled(True)
		self.button_delete.released.connect(self.delInfiniteLine)
		boxTop.addWidget(self.button_delete)

		# spinbox : selection of the temperature point
		self.sb_measurementnbr = QSpinBox()
		self.sb_measurementnbr.setValue(0)
		self.sb_measurementnbr.setMinimum(0)
		self.sb_measurementnbr.setMaximum(0)
		self.sb_measurementnbr.setSingleStep(1)
		self.sb_measurementnbr.setDisabled(True)
		self.sb_measurementnbr.valueChanged.connect(self.measurementnbrChangedEvent)
		boxTop.addWidget(self.sb_measurementnbr)

		# display of temperature point header line
		self.qlabel1 = QLabel()
		self.qlabel1.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
		self.qlabel1.setText("Please drag and drop impedance file into this window")
		boxTop.addWidget(self.qlabel1)

		# separator
		self.qlabel2 = QLabel()
		self.qlabel2.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
		boxTop.addWidget(self.qlabel2)

		# removed points indicators
		self.removedPointsLabel = QLabel()
		self.removedPointsLabel.setObjectName("removedPointsLabel")
		boxTop.addWidget(self.removedPointsLabel)

		# pushbutton "Filter"
		self.removeBadPointsBtn = QToolButton()
		self.removeBadPointsBtn.setFocusPolicy(Qt.TabFocus)
		self.removeBadPointsBtn.setCheckable(True)
		self.removeBadPointsBtn.setText(u'Filter')
		self.removeBadPointsBtn.setToolTip(u"Attemp to automatically remove bad points")
		self.removeBadPointsBtn.toggled.connect(self.automaticallyRemoveBadPointsButtonClicked)
		self.removeBadPointsBtn.setDisabled(True)
		boxTop.addWidget(self.removeBadPointsBtn)

		# surface and thickness
		self.surface = QLineEdit()
		self.surface.setPlaceholderText(u"Surface (cm²)")
		self.surface.setToolTip(u"Please enter the surface (in cm²) of the sample, if you want your graphs labeled in Ω.cm\n(or ASR in Ω.cm² if thickness isn't entered)")
		self.surface.setMaximumWidth(120);
		self.surface.editingFinished.connect(self.sampleGeometryParametersChanged)
		boxTop.addWidget(self.surface)

		self.thickness = QLineEdit()
		self.thickness.setPlaceholderText(u"Thickness (mm)")
		self.thickness.setToolTip(u"Please enter the thickness (in mm) of the sample, if you want your graphs labeled in ohms.cm\nIf not, the ASR will be calculated (s/2)\nOf course, only have effect if surface value is entered.")
		self.thickness.setMaximumWidth(120);
		self.thickness.editingFinished.connect(self.sampleGeometryParametersChanged)
		boxTop.addWidget(self.thickness)
		self.formfactor_unit = u"Ω"
		self.formfactor_value = 1

		# export button
		popupExportMenu = QMenu()
		popupExportMenu.addAction(u'Standard ASCII').triggered.connect(lambda: self.exportFileDialog(DEFAULT_EXPORT))
		popupExportMenu.addAction(u'ECLab format').triggered.connect(lambda: self.exportFileDialog(EC_LAB_EXPORT))
		popupExportMenu.addAction(u'IMPEDA file').triggered.connect(self.exportToImpeda)
		self.button_export = QPushButton()
		self.button_export.setFocusPolicy(Qt.TabFocus)
		self.button_export.setText(u'Export')
		self.button_export.setMenu(popupExportMenu)
		self.button_export.setToolTip(u"Export data at displayed temperature points")
		self.button_export.setDisabled(True)
		boxTop.addWidget(self.button_export)

		# now adding top part to the vertical layout
		mainvboxLayout.addLayout(boxTop)
		mainvboxLayout.addWidget(self.blackWidget)
		QHBoxLayout(self.blackWidget).addWidget(splitterMiddle)

		## Maximize, Minimum size when resized, and window title
		self.setWindowState(self.windowState() ^ Qt.WindowMaximized)
		self.setMinimumSize(800, 600)
		self.setWindowTitleWithOptionalFileName()

		# Nyquist plot(s)
		self.pw_nyquist = pyqtgraph.PlotWidget()
		self.pw_nyquist.setTitle("Nyquist")
		self.pw_nyquist.setLabel('left', text='Imaginary', units=u'Ω.j')
		self.pw_nyquist.setLabel('bottom', text='Real', units=u'Ω')
		self.pw_nyquist.showGrid(1, 1)

		# Bode (magnitude)
		self.pw_bodemag = pyqtgraph.PlotWidget()
		self.pw_bodemag.setTitle("Bode (Magnitude)")
		self.pw_bodemag.setLabel('left', text='Magnitude', units=u'Ω')
		self.pw_bodemag.setLabel('bottom', text='Frequency', units='Hz')
		self.pw_bodemag.setLogMode(x=True, y=None)
		self.pw_bodemag.showGrid(1, 1)

		# Bode (phase)
		self.pw_bodeph = pyqtgraph.PlotWidget()
		self.pw_bodeph.setTitle("Bode (Phase)")
		self.pw_bodeph.setLabel('left', text='Phase', units=u'°')
		self.pw_bodeph.setLabel('bottom', text='Frequency', units='Hz')
		self.pw_bodeph.setLogMode(x=True, y=None)
		self.pw_bodeph.showGrid(1, 1)
		self.pw_bodeph.setXLink(self.pw_bodemag)
		self.pw_bodeph.setYRange(-90, 90)
		self.pw_bodeph.getAxis('left').setTickSpacing(45, 22.5)

		# Temperature
		self.pw_temperature = pyqtgraph.PlotWidget()
		self.pw_temperature.setTitle("Measurements temperature")
		self.pw_temperature.setLabel('left', text=u'Sample temperature (°C)', units='')
		self.pw_temperature.setLabel('bottom', text='Mesurements (#)', units='')
		self.pw_temperature.setLimits(xMin=-1)
		self.pw_temperature.showGrid(1, 1)

		# Add graphs to splitters
		splitterLeft.addWidget(self.pw_nyquist)
		splitterLeft.addWidget(self.pw_temperature)
		splitterRight.addWidget(self.pw_bodemag)
		splitterRight.addWidget(self.pw_bodeph)

		# Set default focus
		self.pw_temperature.setFocus()

		# Enable dragging and dropping onto the GUI
		self.setAcceptDrops(True)

		# Set the icon
		self.setWindowIcon(getEmbeddedIcon())

		# Show the window!
		self.show()

	def autoRange(self):
		for plot in self.pw_nyquist, self.pw_bodemag, self.pw_temperature:
			plot.enableAutoRange()
		self.pw_bodeph.setYRange(-90, 90)

	def keyPressEvent(self, event):
		key = event.key()
		delta = 0
		if key == Qt.Key_Left:
			delta = -1
		elif key == Qt.Key_Right:
			delta = 1
		elif key == Qt.Key_Down:
			delta = -2
		elif key == Qt.Key_Up:
			delta = 2
		elif key == Qt.Key_F5 or key == Qt.Key_Refresh or (key == Qt.Key_R and event.modifiers() & Qt.CTRL):
			if self.filename != "":
				self.loadImpedanceFile(self.filename)
		elif key == Qt.Key_A and event.modifiers() & Qt.CTRL:
			self.autoRange()
		elif key == Qt.Key_Plus:
			self.addInfiniteLine()
		elif key == Qt.Key_Minus:
			self.delInfiniteLine()

		if delta == 0:
			return

		if event.modifiers() & Qt.SHIFT:
			if len(self.infline) == 0:
				return

			self.inflineCurrentlyDragged = True
			for i in self.infline:
				i.setValue(i.value() + delta)
			self.sb_measurementnbr.setValue(self.sb_measurementnbr.value() + delta)
			self.inflineCurrentlyDragged = False
			self.showMarkedImpedancesGraphs()
		else:
			self.sb_measurementnbr.setValue(self.sb_measurementnbr.value() + delta)

	def measurementnbrChangedEvent(self):
		pt=self.sb_measurementnbr.value()-1
		self.qlabel1.setText(self.temperaturepoints_headers[pt])
		if not self.inflineCurrentlyDragged:
			self.inflineLastDragged.setPos(pt+1)
			self.showMarkedImpedancesGraphs()

	def oneButtonModeToggled(self):
		for plot in self.pw_nyquist, self.pw_bodeph, self.pw_bodemag, self.pw_temperature:
			viewBox = plot.getViewBox();
			if self.button_onebuttonmode.isChecked() == True:
				viewBox.setMouseMode(viewBox.RectMode)
			else:
				viewBox.setMouseMode(viewBox.PanMode)
				if plot != self.pw_bodeph:
					plot.enableAutoRange()
				else:
					self.pw_bodeph.setYRange(-90, 90)

	def measurementDragged(self, element):
		x = int(round(element.value()))

		# doesn't update if not significally changed position
		try:
			if x != self.lastDraggedValue:
				self.lastDraggedValue = x
			else:
				element.label.setText(u"#%d: %g°C" % (x, self.temperaturepointsY[x-1]))
				return
		except:
			self.lastDraggedValue = x

		# if shift is pressed, move all the others vertical lines line along this one
		if not self.inflineCurrentlyDragged:
			self.inflineCurrentlyDragged = True
			self.inflineCurrentlyDraggedOrigin = x
		else:
			if isShiftPressed() and (x != self.inflineCurrentlyDraggedOrigin):
				for i in self.infline:
					if i != element:
						i.setValue(i.value() + x - self.inflineCurrentlyDraggedOrigin)

		self.sb_measurementnbr.setValue(x)

		element.label.setText(u"#%d: %g°C" % (x, self.temperaturepointsY[x-1]))
		if len(self.infline)<=3:
			self.showMarkedImpedancesGraphs()
		else:
			self.showImpedanceGraphs(x-1)
		self.inflineCurrentlyDraggedOrigin = x

	def measurementPositionChangeFinished(self, element):
		x = round(element.value())
		self.inflineLastDragged = element
		element.setPos(x)
		self.sb_measurementnbr.setValue(x)
		self.inflineCurrentlyDragged = False
		self.lastDraggedValue = None
		self.showMarkedImpedancesGraphs()

	def sampleGeometryParametersChanged(self):
		s = 0; t = 0
		try:
			if self.surface.text() != "":
				s = float(self.surface.text())
				if s == 0:
					self.surface.setText("")
			if self.thickness.text() != "":
				t = float(self.thickness.text())
				if t == 0:
					self.thickness.setText("")

			if s == 0:
				self.formfactor_unit = u"Ω"
				self.formfactor_value = 1
			elif t == 0:
				self.formfactor_unit = u"Ω.cm²"
				self.formfactor_value = s/2
			else:
				self.formfactor_unit = u"Ω.cm"
				self.formfactor_value = s/(t/10) # 10 due to the thickness conversion from mm to cm

			self.pw_nyquist.setLabel('left', text='Imaginary', units=self.formfactor_unit+u'.j')
			self.pw_bodemag.setLabel('left', text='Magnitude', units=self.formfactor_unit)
			self.pw_nyquist.setLabel('bottom', text='Real', units=self.formfactor_unit)
			self.showMarkedImpedancesGraphs()

		except ValueError:
			QMessageBox.warning(self, "User error", "The sample surface and/or thickness parameters are not valid numbers!")

	def showMarkedImpedancesGraphs(self):
		pts = []
		for element in self.infline:
			index = int(round(element.value())-1)
			pts.append(index)
			element.label.setText(u"#%d: %g°C" % (index+1, self.temperaturepointsY[index]))
		self.showImpedancesGraphs(pts)

	def delInfiniteLine(self):
		if len(self.infline) > 1:
			self.infline[-1].sigDragged.disconnect(self.measurementDragged)
			self.infline[-1].sigPositionChangeFinished.disconnect(self.measurementPositionChangeFinished)
			self.pw_temperature.removeItem(self.infline[-1])
			del self.infline[-1]
			if len(self.infline) < 2:
				self.button_delete.setDisabled(True)

			self.inflineCurrentlyDragged = True
			self.sb_measurementnbr.setValue(round(self.infline[-1].value()))
			self.inflineLastDragged = self.infline[-1]
			self.inflineCurrentlyDragged = False
			self.showMarkedImpedancesGraphs()

	def automaticallyRemoveBadPointsButtonClicked(self):
		if self.removeBadPointsBtn.isChecked():
			autoRangeState = self.pw_nyquist.plotItem.vb.state['autoRange']
			self.pw_nyquist.plotItem.vb.state['autoRange'] = [False, False]

		self.measurementnbrChangedEvent()

		if self.removeBadPointsBtn.isChecked():
			self.pw_nyquist.plotItem.vb.state['autoRange'] = autoRangeState

	def automaticallyRemoveBadPointsFunction(self, pt, p):
		frequencies, reals, imaginaries, magnitudes, phases = p
		removedDataPoints = set()

		def remove(i):
			removedDataPoints.add(i)
			reals[i]       = float("nan")
			imaginaries[i] = float("nan")
			magnitudes[i]  = float("nan")

		def getNext(i):
			while i < len(frequencies)-1:
				i+=1
				if not (math.isnan(frequencies[i]) or math.isnan(imaginaries[i]) or math.isnan(magnitudes[i])):
					return i
			return None

		def getPrevious(i):
			while i > 0:
				i-=1
				if not (math.isnan(frequencies[i]) or math.isnan(imaginaries[i]) or math.isnan(magnitudes[i])):
					return i
			return None

		# remove all "nan" points and computes the median value of impedance magnitudes
		magnitudesNotNAN = []
		for i in range(len(frequencies)):
			if math.isnan(frequencies[i]) or math.isnan(imaginaries[i]) or math.isnan(magnitudes[i]):
				remove(i)
			else:
				magnitudesNotNAN.append(magnitudes[i])
		medMagImped = sorted(magnitudesNotNAN)[int(len(magnitudesNotNAN)/2)]

		print()
		print("Median impedance magnitude:", medMagImped)

		# decides for a typical, smooth index to start looking flatness
		startScores = {}
		for i in range(4, len(frequencies) - 4):
			if math.isnan(magnitudes[i]):
				continue
			values = []
			for j in range(i-3, i+3):
				if j>=0 and j<len(frequencies) and not math.isnan(magnitudes[j]):
					values.append((magnitudes[j] - medMagImped) ** 2)
			startScores[i] = sum(values) / len(values)

		if len(startScores) < 20:
			print("Too few values to attempt filtering")
			return

		startIndex = sorted(startScores.items(), key = lambda kv:(kv[1], kv[0]))[0][0]; del startScores
		print("Start point index:", startIndex)

		# calculate some "score" for both halves, we starts by left side
		badnessScoresLeft = {}
		def calculateBadnessLeft(i):
			i1 = getNext(i)
			if i1 == None:
				return
			df = math.log10(frequencies[i1]) - math.log10(frequencies[i])
			badnessScoresLeft[i] = abs(magnitudes[i] - magnitudes[i1]) / df

		for i in range(startIndex-1, -1, -1):
			calculateBadnessLeft(i)

		# then we continues by the right side
		badnessScoresRight = {}
		def calculateBadnessRight(i):
			i1 = getPrevious(i)
			if i1 == None:
				return
			df = math.log10(frequencies[i]) - math.log10(frequencies[i1])
			badnessScoresRight[i] = abs(magnitudes[i] - magnitudes[i1]) / df

		for i in range(startIndex+1, len(frequencies)):
			calculateBadnessRight(i)

		# statistically choose a threshold (will be used for each sides)
		def calculateThreshold(data):
			sortedData = sorted(data.items(), key = lambda kv:(kv[1], kv[0]), reverse=True)
			decisionIndex = int(len(sortedData) * 0.15)
			threshold = sortedData[decisionIndex][1] * 5
			return threshold

		# remove bad points in left part
		threshold = calculateThreshold(badnessScoresLeft)
		print("Threshold for left part:", threshold)
		for i in range(startIndex-1, -1, -1):
			if badnessScoresLeft[i] > threshold:
				remove(i)
				self.removedPointsNumber+=1
				i1 = getPrevious(i)
				if i1 != None:
					calculateBadnessLeft(i1)

		# remove bad points in right part
		threshold = calculateThreshold(badnessScoresRight)
		print("Threshold for right part:", threshold)
		for i in range(startIndex+1, len(frequencies)):
			if badnessScoresRight[i] > threshold:
				remove(i)
				self.removedPointsNumber+=1
				i1 = getNext(i)
				if i1 != None:
					calculateBadnessRight(i1)
		print("Deleted points:", removedDataPoints)
		return removedDataPoints

	def exportFileDialog(self, mode):
		txt = "Select a directory to export"
		if mode == EC_LAB_EXPORT:
			txt+= " (ECLab format)"
		directory = str_f(QFileDialog.getExistingDirectory(self, txt))
		if (directory == ""):
			return
		try:
			files=0
			prefixname = os.path.splitext(os.path.basename(self.filename))[0]
			for i in self.infline:
				pt = int(round(i.value())-1)
				name = prefixname+"_"+format(pt+1, "03d")+"_"+format(self.temperaturepointsY[pt], ".0f")
				if self.formfactor_value != 1:
					name+="_"+format(self.formfactor_value, "0.3f")
				fd = open(os.path.join(directory, name)+".txt", "wb")

				if mode == EC_LAB_EXPORT:
					fd.write("freq/Hz\tRe(Z)/Ohm\t-Im(Z)/Ohm\r\n".encode("UTF-8"))
				for i in range(len(self.measurements_freq[pt])):
					if mode == EC_LAB_EXPORT:
						fd.write((format(self.measurements_freq[pt][i], ".4e")+"\t"+format(self.measurements_Zreal[pt][i]*self.formfactor_value, ".10e")+"\t"+format(-self.measurements_Zim[pt][i]*self.formfactor_value, ".10e")+"\r\n").encode("UTF-8"))
					else:
						fd.write((format(self.measurements_freq[pt][i], ".4e")+"\t"+format(self.measurements_Zreal[pt][i]*self.formfactor_value, ".10e")+"\t"+format(self.measurements_Zim[pt][i]*self.formfactor_value, ".10e")+"\n").encode("UTF-8"))
				fd.close()
				files+=1
			QMessageBox.information(self, "Export", "Export finished, "+str(files)+" file(s) processed.")
		except Exception as e:
			QMessageBox.critical(self, "Export error", sys.exc_info()[0].__name__ + ", line " + str(sys.exc_info()[2].tb_lineno)+"\n"+str(e))

	def exportToImpeda(self):
		try:
			barefile = os.path.splitext(os.path.basename(self.filename))[0]
			dialog = QFileDialog(self, "Exporting to IMPEDA...")
			dialog.setFilter(dialog.filter() | QDir.Hidden)
			dialog.setDefaultSuffix('')
			dialog.selectFile(barefile)
			dialog.setAcceptMode(QFileDialog.AcceptSave)
			dialog.setNameFilters(["IMPEDA file (%s*)" % barefile, "All files (*)"])

			if dialog.exec_() == QDialog.Accepted:
				filename = str_f(dialog.selectedFiles()[0])
				fd = open(filename, "wb")
				for pt, temperature in enumerate(self.temperaturepointsY):
					data_out = ""
					data_lines = 0
					for freq, z_re, z_im in zip(self.measurements_freq[pt], self.measurements_Zreal[pt], self.measurements_Zim[pt]):
						# IMPEDA will do the inverse calculation with a dummy one-ohm resistor
						if math.isnan(z_re) or math.isnan(z_im):
							continue
						data_out+= "%e %e %e\r\n" % (freq, z_re+1, z_im); data_lines+= 1

					if data_lines >= 1:
						# IMPEDA dummy one-ohm resistor
						fd.write(("%g %u 1\r\n" % (temperature, data_lines) + data_out).encode("ascii"))

				fd.close()
				QMessageBox.information(self, "Export", "Exported IMPEDA file:\n%s" % filename)
			else:
				return
		except Exception as e:
			QMessageBox.critical(self, "Export error", "%s, line %u\n%s" % (sys.exc_info()[0].__name__, sys.exc_info()[2].tb_lineno, e))


	def addInfiniteLine(self):
		self.inflineCurrentlyDragged = True
		i = len(self.infline)
		pos = 0.1 + 0.8 * ((i%11)/10.0)
		self.infline.append(pyqtgraph.InfiniteLine(movable=True, angle=90, label=u'#{value:0.0f}', pen=self.colorChoice(i),
                    labelOpts={'position':pos, 'color': self.colorChoice(i), 'fill': (200,200,200,50), 'movable': True}))
		try:
			maxpoint = len(self.temperaturepointsX)+1
		except:
			maxpoint = 1

		if i>0:
			self.button_delete.setDisabled(False)

		self.infline[i].setBounds([1,maxpoint-1])
		x = self.sb_measurementnbr.value()+1
		self.infline[i].setPos(x)
		self.sb_measurementnbr.setValue(x)

		self.infline[i].sigDragged.connect(self.measurementDragged)
		self.infline[i].sigPositionChangeFinished.connect(self.measurementPositionChangeFinished)
		self.pw_temperature.addItem(self.infline[i])
		self.inflineLastDragged = self.infline[i]
		self.inflineCurrentlyDragged = False
		self.showMarkedImpedancesGraphs()

	# The following three methods set up dragging and dropping for the app
	def dragEnterEvent(self, e):
		if e.mimeData().hasUrls:
			e.accept()
		else:
			e.ignore()

	def dragMoveEvent(self, e):
		if e.mimeData().hasUrls:
			e.accept()
		else:
			e.ignore()

	def dropEvent(self, e):
		# Drop files directly onto the widget
		if e.mimeData().hasUrls:
			e.setDropAction(Qt.CopyAction)
			e.accept()
			for url in e.mimeData().urls():
				self.loadImpedanceFile(url.toLocalFile())
		else:
			e.ignore()

	def setWindowTitleWithOptionalFileName(self, filename=""):
		if filename:
			self.setWindowTitle(os.path.basename(str_u(filename)) + " - " + PGRNAME)
		else:
			self.setWindowTitle(PGRNAME + " " + VERSION + " (" + QT_VERSION + " with Python " + sys.version.split()[0] + ")")

	def loadImpedanceFile(self, filename):
		self.setWindowTitleWithOptionalFileName()
		try:
			self.filename = str_f(filename)
			print("*** "+self.filename+" ***")
			fd = open(self.filename, 'r')
			datafile = fd.read().split("\n")
			fd.close()
			temperature_point_number = 0
			frequency_points_remaining = 0
			frequency_point_number = 0
			self.temperaturepoints_headers = {}
			self.temperaturepointsY  = []
			self.temperaturepointsX  = []
			self.measurements_freq   = {}
			self.measurements_Zreal  = {}
			self.measurements_Zim    = {}
			self.measurements_Zmag   = {}
			self.measurements_Zphase = {}

			for line in datafile:
				line = line.replace("\r", "")
				line_data = line.split()

				# end of file and such
				if len(line_data) < 3:
					continue

				if frequency_points_remaining != 0 and len(line_data) > 3:
					print(("Pt. "+ str(temperature_point_number+1)+" ("+str(temperature)+u"°C) : "+str(frequency_points_remaining)+" datapoint(s) missing").encode('utf-8'))
					frequency_points_remaining = 0

				if frequency_points_remaining == 0:
					# this is a header line at each temperature point
					if frequency_point_number != 0:
						temperature_point_number+=1

					self.temperaturepoints_headers[temperature_point_number] = line
					self.measurements_freq[temperature_point_number]         = []
					self.measurements_Zreal[temperature_point_number]        = []
					self.measurements_Zim[temperature_point_number]          = []
					self.measurements_Zmag[temperature_point_number]         = []
					self.measurements_Zphase[temperature_point_number]       = []

					temperature = float(line_data[0])
					if numpy.isnan(temperature):
						temperature = 0

					frequency_points_remaining = int(line_data[1])

					# l'enfer, c'est l'existant :x
					resistor = float(line_data[2])
					if numpy.isnan(resistor):
						resistor = 1

					frequency_point_number = 0
					self.temperaturepointsX.append(temperature_point_number+1)
					self.temperaturepointsY.append(temperature)
				else:
					# frequency point line
					frequency = float(line_data[0])
					Zreal = float(line_data[1])
					Zim = float(line_data[2])

					if resistor != 1:
						Zreal = (Zreal-1) * resistor;
						Zim   =  Zim      * resistor;

					Zmag = numpy.sqrt(Zreal**2 + Zim**2)
					Zphase = math.atan2(Zim, Zreal)*180.0/math.pi

					self.measurements_freq[temperature_point_number].append(frequency)
					self.measurements_Zreal[temperature_point_number].append(Zreal)
					self.measurements_Zim[temperature_point_number].append(Zim)
					self.measurements_Zphase[temperature_point_number].append(Zphase)
					self.measurements_Zmag[temperature_point_number].append(Zmag)

					frequency_point_number+=1
					frequency_points_remaining-=1

			if len(self.infline) == 0:
				self.addInfiniteLine()

			# update controls
			self.sb_measurementnbr.setMaximum(temperature_point_number+1)
			self.sb_measurementnbr.setMinimum(1)
			self.sb_measurementnbr.setSuffix("/"+str(temperature_point_number+1))
			self.button_nyquistzero.setDisabled(False)
			self.button_add.setDisabled(False)
			self.removeBadPointsBtn.setDisabled(False)
			self.sb_measurementnbr.setDisabled(False)
			self.button_export.setDisabled(False)

			# update temperature graph
			fg = '#ffffff'
			if self.button_whitetheme.isChecked():
				fg = '#000000'

			self.pw_temperature.clear()
			self.pw_temperature.plot(self.temperaturepointsX, self.temperaturepointsY, pen=fg, connect='finite', symbol='x')
			self.pw_temperature.setLimits(xMax=temperature_point_number+10.5)
			self.pw_temperature.getAxis('left').setWidth(60)
			self.pw_temperature.enableAutoRange()

			for infline in self.infline:
				self.pw_temperature.addItem(infline)
				infline.setBounds([1,len(self.temperaturepointsX)])

			self.qlabel1.setText(self.temperaturepoints_headers[self.sb_measurementnbr.value()-1])

			# .. and others
			self.showMarkedImpedancesGraphs()
			self.setWindowTitleWithOptionalFileName(self.filename)

		except Exception as e:
			QMessageBox.critical(self, "File loading error", sys.exc_info()[0].__name__ + ", line " + str(sys.exc_info()[2].tb_lineno)+"\n"+str(e))
			self.filename = ""
			self.button_export.setDisabled(True)

	def showImpedanceGraphs(self, pt):
		self.showImpedancesGraphs([pt])

	def showImpedancesGraphs(self, pts):
		self.removedPointsNumber = 0
		self.pw_nyquist.clear()
		if self.button_nyquistzero.isChecked() == True:
			self.pw_nyquist.plot([0],[0]) # VDT wanted this.

		self.pw_bodemag.clear()
		self.pw_bodeph.clear()
		i=0
		for pt in pts:
			pencolor = '#ffffff'
			if self.button_whitetheme.isChecked():
				pencolor = '#000000'
			nyquist_symbol = None
			if len(pts) <= 2:
				nyquist_symbol = 'x'
			if len(pts) >= 2:
				pencolor = self.colorChoice(i)

			frequencies = numpy.array(self.measurements_freq[pt])
			reals       = numpy.array(self.measurements_Zreal[pt]) * self.formfactor_value
			imaginaries = numpy.array(self.measurements_Zim[pt])   * self.formfactor_value
			magnitudes  = numpy.array(self.measurements_Zmag[pt])  * self.formfactor_value
			phases      = numpy.array(self.measurements_Zphase[pt])

			if self.removeBadPointsBtn.isChecked() and self.inflineCurrentlyDragged == False:
				self.automaticallyRemoveBadPointsFunction(pt, [frequencies, reals, imaginaries, magnitudes, phases])
				connect = 'all'
			else:
				connect = 'finite'

			# Nyquist
			self.pw_nyquist.plot(reals, imaginaries, pen=pencolor, connect=connect, symbol=nyquist_symbol)
			self.pw_nyquist.getAxis('left').setWidth(60)
			self.pw_nyquist.setAspectLocked(lock=True, ratio=1)
			self.pw_nyquist.getViewBox().invertY(True)

			# Bode (magnitude)
			self.pw_bodemag.plot(frequencies, magnitudes, pen=pencolor, connect=connect)
			self.pw_bodemag.getAxis('left').setWidth(60)
			self.pw_bodemag.getAxis('left').enableAutoSIPrefix(True)
			self.pw_bodemag.getAxis('bottom').enableAutoSIPrefix(True)

			# Bode (phase)
			self.pw_bodeph.plot(frequencies, phases, pen=pencolor, connect=connect)
			self.pw_bodeph.setYRange(-90, 90, padding=0)
			self.pw_bodeph.getAxis('left').setWidth(60)
			self.pw_bodeph.getAxis('bottom').enableAutoSIPrefix(True)

			i+=1

		if self.removedPointsNumber > 0:
			self.removedPointsLabel.setText(str(self.removedPointsNumber))
		else:
			self.removedPointsLabel.setText("")


def getEmbeddedIcon():
	s = """
	iVBORw0KGgoAAAANSUhEUgAAAOoAAADqAgMAAADR+jHdAAAADFBMVEUAAgA2NzWEg5ny8/Nbt2NaAAAFUklEQVRo3u2bvW7kNhDHh6KBExTCMC5NEFyxwHXaFMRVjos8QxqnSJ/mnoIHdwfjumsD
	IW6I9cJwUqYIdB8IgqvSpHe58FNEnyuJM6RpLnfXBryVPeaP/+VoOBqOZIC9fxiyHCDL0RMbyJ4Gf3700E1BBX/nPCmC2extuG6y2MDPGvaim/21gZ9/CPdz+ilcFz7BXlgO5aNjBchHx8qKfmxs
	WV2lMDaBG3iZhLGZWsFlpsJ0LwrIbwPXu1CQ6UB2XmkXu9dNrqr1ngT6+VyDPgn0s5zB7DBwvbLZSrtnyybt7JzlsCG7zpS7Y8XGrNw5KzdmBcmmblYdO9jcySZFm6rOU4rVbl29asqki5xiCzeb
	t3+/1BSr3GyXmbspgnT75D5lV262+7K07q3bz8vWSXpFscdu3fbiJMUJxUo32wYFU98HsHI6bMoKP1bsmC1dLPdj+W7Z4a8llTfKh8gKI4YMVnqx9S0csZ2RZocbfnatMdsZaXYoNOqSBeteFXZ2
	KHDqUgmvd6Hs7FBYdbnYYHOHr0Yn7g1065IUr/fCtd51NsvOKD+fOfw8FM51CY51WyPNDgV7F3/mPbS0swLisJJi5XZY+QBYQbHCzpYPgOUUy63sM9gLmz5TZvyhmrC0sPlh4WZPT0//a/t+P5mN
	wJ9/fT/+tR72i9EnTEtF646bXt0w8zvnrwqLrzQaZrLZn166zbAjyxDE9pOOhiE/L21+/rswh3nrpp/v1M3eWdYLn9Ew5Gdu8TM/QMNMNuXKh22GIV9x8GGbYaFsHdD4fFTSrIjIytispFkZkRWx
	WUGzZUSWx2Y5xVbxa7BVBeHJVvvGYPUXhdm2JDHY7K2pW1VMmK0mxGyVJwy2qtQw25ZgDKUxg02uCbYt/cJ0qwmJ9b5D6819dav7gOnnQ8LPbYltXl9p6lYnAMLPh4Sfqx1ygOIAs+2RgqGM4MO2
	AR3IcoLlEJWFkupBlXFZEZmVFCvjsjIyKyhWbMDWLR+Gjk1ebNOPm7D14wzMvnxO6NYtrqnubwXBXn6rMFv3xRg6niJ2nhO6WWmycyDYTEOwblIQbN2PY6gN4KXb9OOmfj7XBDvqHYw6nMemrpgR
	7NCzMKKIoShF7OwV0EHpwcKLrbOlP8v92K99WA7R2aMSyGwXjRVbYCWQgRWLbR9JEOx1SuwkOWPGvqLY50sic1R7bqyrtUV3Se3gq4IZeYRkcypzLBQz8hfJZtQGnkMs3UvbevWKujOM19s9tyH8
	XNwSflarse5ZatFV/xC6cMPQKYB6D4oOrO2z4k5W3o8V22c5mbEYOtXGZqGMzMIH1D2IzQ7PFMZR+WHmww7PFMa74XdttixJ3T+IHTwy9o8zyPUuFLn7p2ceC5tP+oTd5/Xw02vHa4SL98T7hINu
	XwqR670oyKwzPVta/KyIjJW9WXnprp8pjHXZzfQsbXv3ksocTE7P8GHs7KNLl9r9TJhd5fgstfsZ92O/AhyVbG0UTjb/iKODrY1uNvtC6K6NbnbaWu7YtVE6WSA2P1sb5dZ0+77xdL29sXT7mVN+
	7o1u9hv4F7NpD7lZKkWz3riOLxsrKVZ2Tb472O8Sik0mvrCxS1Shsc44XAMbi4+yrDMO197G4iMl643Lu1h8lGWdcRPd4TGKjc2p9Tbn22Fv21hN+bm5ifXPouzsLClTxM74JJdZ36tPrnPEVlGZ
	+rDdeWLK1hMKD9Ys31k3oQ9rOot1EwbrVhPKwPXWE3qwSUH5WV+N7s12XZUSfi7yUU1wz/+5UNmoFrn3/2uMaq97s3PYi27TqAtkM6XDdWEWvl6AJ/aJrT//A8QCjONU/GkQAAAAAElFTkSuQmCC
	"""
	qpm = QPixmap()
	icon = QIcon()
	qba_s = QByteArray(bytes(s.encode()))
	qba = QByteArray.fromBase64(qba_s)
	qpm.convertFromImage(QImage.fromData(qba.data(), 'PNG'))
	icon.addPixmap(qpm)
	return icon

def main():
	try: # https://github.com/pyqtgraph/pyqtgraph/issues/756
		QApplication.setHighDpiScaleFactorRoundingPolicy(Qt.HighDpiScaleFactorRoundingPolicy.PassThrough)
		QCoreApplication.setAttribute(Qt.AA_EnableHighDpiScaling, True)
	except:
		pass

	app = QApplication(sys.argv)
	m1 = ImpedanceViewerGUI(app)
	sys.exit(app.exec_())

if __name__ == '__main__':
	main()

